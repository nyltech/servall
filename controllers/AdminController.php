<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use webvimark\modules\UserManagement\models\rbacDB\Role;
use yii\helpers\Json;
use yii\web\UploadedFile;
use app\models\Customerdetails;
use app\models\Customerdetailssearch;

use yii\helpers\ArrayHelper;


class AdminController extends Controller
{
    public function behaviors()
    {
        return [
            'ghost-access' => [
                'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('role');
    }

    public function actionCustomerdetails(){
        $getCustomerdetails = new Customerdetailssearch();
        $dataProvider = $getCustomerdetails->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 10;
        $dataProvider->setSort([
            'defaultOrder' => ['id' => SORT_DESC]]);
        return $this->render('customerdetails', [
                    'searchModel' => $getCustomerdetails,
                    'dataProvider' => $dataProvider
        ]);

    }

    

 

   
}
?>