<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\ActiveDataProvider;
use yii\base\ErrorException;
use app\models\Customerdetails;
use app\models\Categorydetails;


class CustomerController extends Controller
{
    public $layout = false;
	
	//Add Customer Details
	public function actionCustomer_details()
    {
        $required_fields['customerName'] = 'Required Customer Name';
        $required_fields['mobileNumber'] = 'Required Mobilenumber';        
        $requireField = Yii::$app->mycomponent->RequiredFieldVaildation($_GET, $required_fields);
        if (!empty($requireField)) {
            $response = ['errorCode' => 1, 'status' => 'required', 'message' => $requireField];
        } else {
            $customerName = $_GET['customerName'];
            $mobileNumber = $_GET['mobileNumber'];                
            $Customer  = new Customerdetails();
            if ($Customer) {
                $Customer->customerName = $customerName;
                $Customer->mobileNumber = $mobileNumber;               			
				 date_default_timezone_set('Asia/Riyadh');
				$date = date('Y-m-d');
				$Customer->createdDate = $date;		
			   $Customer->encryptionId = Yii::$app->getSecurity()->generateRandomString();
                if ($Customer->save()) {
                    $response = ['errorCode' => 0, 'Status' => 'Success', 'Message' => 'Customer Added Successfully'];
                }
            } else {
                $response = ['errorCode' => 2, 'Status' => 'Failed', 'Message' => 'Failed'];
            }
        }
        return Json::encode($response);
    }
	
	//Get City Details
	public function actionCity_details()
    {                       
            $Get_city  = Citydetails::find()->all();
            if ($Get_city) {				
				foreach($Get_city as $city){
					$result[] = [
					'cityIndexId'=>$city->id,
					'cityName'=>$city->cityName					
					];					
					 $response = ['errorCode' => 0, 'Status' => 'Success', 'Message' => 'Getting Records Successfully','results'=>$result];
				   }               
                   }else {
                $response = ['errorCode' => 2, 'Status' => 'Failed', 'Message' => 'No Records Found'];
				}        
        return Json::encode($response);
    }
	
	//Get Category Details
	public function actionCategory_details()
    {        
        $required_fields['cityIndexId'] = 'City Id is required';
        $requireField = Yii::$app->mycomponent->RequiredFieldVaildation($_POST, $required_fields);
        if (!empty($requireField)) {
            $response = ['errorCode' => 1, 'status' => 'required', 'Message' => $requireField];
        } else {
			$cityId = $_POST['cityIndexId'];			
            $Get_category  = Categorydetails::find()->where(['id'=>$cityId])->all();
            if ($Get_category) {				
				foreach($Get_category as $category){
					$result[] = [
					'categoryIndexId'=>$category->id,
					'categoryName'=>$category->categoryName,					
					'categoryImage'=>'https://'.$_SERVER['HTTP_HOST'].Yii::$app->request->baseUrl . '/images/category_images/' . $category['categoryImage'],				
					'createdDate'=>$category->createdDate					
					];					
					 $response = ['errorCode' => 0, 'Status' => 'Success', 'Message' => 'Getting Records Successfully','results'=>$result];
				   }               
                   }else {
                $response = ['errorCode' => 2, 'Status' => 'Failed', 'Message' => 'No Records Found'];
				}      
		}				
        return Json::encode($response);
    }
	
	
	
	

}
?>