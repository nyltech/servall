-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 10, 2019 at 08:21 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nylte2hv_servall`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `rule_name` varchar(64) DEFAULT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `group_code` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`, `group_code`) VALUES
('/*', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('//*', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('//controller', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('//crud', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('//extension', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('//form', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('//index', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('//model', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('//module', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/asset/*', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/asset/compress', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/asset/template', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/cache/*', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/cache/flush', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/cache/flush-all', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/cache/flush-schema', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/cache/index', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/fixture/*', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/fixture/load', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/fixture/unload', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/gii/*', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/gii/default/*', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/gii/default/action', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/gii/default/diff', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/gii/default/index', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/gii/default/preview', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/gii/default/view', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/hello/*', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/hello/index', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/help/*', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/help/index', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/help/list', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/help/list-action-options', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/help/usage', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/message/*', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/message/config', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/message/config-template', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/message/extract', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/migrate/*', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/migrate/create', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/migrate/down', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/migrate/fresh', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/migrate/history', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/migrate/mark', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/migrate/new', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/migrate/redo', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/migrate/to', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/migrate/up', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/serve/*', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/serve/index', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/user-management/*', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/user-management/auth/change-own-password', 3, NULL, NULL, NULL, 1535303632, 1535303632, NULL),
('/user-management/user-permission/set', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/user-management/user-permission/set-roles', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/user-management/user/bulk-activate', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/user-management/user/bulk-deactivate', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/user-management/user/bulk-delete', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/user-management/user/change-password', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/user-management/user/create', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/user-management/user/delete', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/user-management/user/grid-page-size', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/user-management/user/index', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/user-management/user/update', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('/user-management/user/view', 3, NULL, NULL, NULL, 1535303631, 1535303631, NULL),
('Admin', 1, 'Admin', NULL, NULL, 1535303631, 1535303631, NULL),
('assignRolesToUsers', 2, 'Assign roles to users', NULL, NULL, 1535303631, 1535303631, 'userManagement'),
('bindUserToIp', 2, 'Bind user to IP', NULL, NULL, 1535303631, 1535303631, 'userManagement'),
('changeOwnPassword', 2, 'Change own password', NULL, NULL, 1535303632, 1535303632, 'userCommonPermissions'),
('changeUserPassword', 2, 'Change user password', NULL, NULL, 1535303631, 1535303631, 'userManagement'),
('commonPermission', 2, 'Common permission', NULL, NULL, 1535303628, 1535303628, NULL),
('createUsers', 2, 'Create users', NULL, NULL, 1535303631, 1535303631, 'userManagement'),
('deleteUsers', 2, 'Delete users', NULL, NULL, 1535303631, 1535303631, 'userManagement'),
('editUserEmail', 2, 'Edit user email', NULL, NULL, 1535303631, 1535303631, 'userManagement'),
('editUsers', 2, 'Edit users', NULL, NULL, 1535303631, 1535303631, 'userManagement'),
('viewRegistrationIp', 2, 'View registration IP', NULL, NULL, 1535303631, 1535303631, 'userManagement'),
('viewUserEmail', 2, 'View user email', NULL, NULL, 1535303631, 1535303631, 'userManagement'),
('viewUserRoles', 2, 'View user roles', NULL, NULL, 1535303631, 1535303631, 'userManagement'),
('viewUsers', 2, 'View users', NULL, NULL, 1535303631, 1535303631, 'userManagement'),
('viewVisitLog', 2, 'View visit log', NULL, NULL, 1535303631, 1535303631, 'userManagement');

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('Admin', 'assignRolesToUsers'),
('Admin', 'changeOwnPassword'),
('Admin', 'changeUserPassword'),
('Admin', 'createUsers'),
('Admin', 'deleteUsers'),
('Admin', 'editUsers'),
('Admin', 'viewUsers'),
('assignRolesToUsers', '/user-management/user-permission/set'),
('assignRolesToUsers', '/user-management/user-permission/set-roles'),
('assignRolesToUsers', 'viewUserRoles'),
('assignRolesToUsers', 'viewUsers'),
('changeOwnPassword', '/user-management/auth/change-own-password'),
('changeUserPassword', '/user-management/user/change-password'),
('changeUserPassword', 'viewUsers'),
('createUsers', '/user-management/user/create'),
('createUsers', 'viewUsers'),
('deleteUsers', '/user-management/user/bulk-delete'),
('deleteUsers', '/user-management/user/delete'),
('deleteUsers', 'viewUsers'),
('editUserEmail', 'viewUserEmail'),
('editUsers', '/user-management/user/bulk-activate'),
('editUsers', '/user-management/user/bulk-deactivate'),
('editUsers', '/user-management/user/update'),
('editUsers', 'viewUsers'),
('viewUsers', '/user-management/user/grid-page-size'),
('viewUsers', '/user-management/user/index'),
('viewUsers', '/user-management/user/view');

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_group`
--

CREATE TABLE `auth_item_group` (
  `code` varchar(64) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_item_group`
--

INSERT INTO `auth_item_group` (`code`, `name`, `created_at`, `updated_at`) VALUES
('userCommonPermissions', 'User common permission', 1535303631, 1535303631),
('userManagement', 'User management', 1535303631, 1535303631);

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1535303603),
('m140608_173539_create_user_table', 1535303625),
('m140611_133903_init_rbac', 1535303626),
('m140808_073114_create_auth_item_group_table', 1535303627),
('m140809_072112_insert_superadmin_to_user', 1535303628),
('m140809_073114_insert_common_permisison_to_auth_item', 1535303628),
('m141023_141535_create_user_visit_log', 1535303628),
('m141116_115804_add_bind_to_ip_and_registration_ip_to_user', 1535303629),
('m141121_194858_split_browser_and_os_column', 1535303629),
('m141201_220516_add_email_and_email_confirmed_to_user', 1535303630),
('m141207_001649_create_basic_user_permissions', 1535303632);

-- --------------------------------------------------------

--
-- Table structure for table `servall_customerdetails`
--

CREATE TABLE `servall_customerdetails` (
  `id` int(11) NOT NULL,
  `customerName` varchar(30) COLLATE utf8_bin NOT NULL,
  `mobileNumber` varchar(20) COLLATE utf8_bin NOT NULL,
  `createdDate` date NOT NULL,
  `encryptionId` mediumtext COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `confirmation_token` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `superadmin` smallint(6) DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `registration_ip` varchar(15) DEFAULT NULL,
  `bind_to_ip` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `email_confirmed` smallint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `confirmation_token`, `status`, `superadmin`, `created_at`, `updated_at`, `registration_ip`, `bind_to_ip`, `email`, `email_confirmed`) VALUES
(1, 'superadmin', 'SCFZ4jhItOaQ0glPlujuVGpGlRoVN_wu', '$2y$13$ULH23aGsxtSikIw/SlybB.G4BMR4J5sAFovSZ.2YUBaIdA/fJUZDi', NULL, 1, 1, 1535303628, 1535303628, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_visit_log`
--

CREATE TABLE `user_visit_log` (
  `id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `language` char(2) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `visit_time` int(11) NOT NULL,
  `browser` varchar(30) DEFAULT NULL,
  `os` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`),
  ADD KEY `fk_auth_item_group_code` (`group_code`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_item_group`
--
ALTER TABLE `auth_item_group`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `servall_customerdetails`
--
ALTER TABLE `servall_customerdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_visit_log`
--
ALTER TABLE `user_visit_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `servall_customerdetails`
--
ALTER TABLE `servall_customerdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_visit_log`
--
ALTER TABLE `user_visit_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_assignment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_auth_item_group_code` FOREIGN KEY (`group_code`) REFERENCES `auth_item_group` (`code`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_visit_log`
--
ALTER TABLE `user_visit_log`
  ADD CONSTRAINT `user_visit_log_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
