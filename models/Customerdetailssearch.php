<?php

namespace app\models;

use Yii;
use yii\base\model;
use yii\data\ActiveDataProvider;
use app\models\Customerdetails;

class Customerdetailssearch extends Customerdetails {


    public function search($params) {
        $query = Customerdetails::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
    
        return $dataProvider;
    }

}
