<?php

namespace app\components;

use Yii;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\BaseHtml;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\base\Component;
use yii\base\InvalidConfigException;
use webvimark\modules\UserManagement\models\rbacDB\Role;
use yii\web\UrlRuleInterface;
use yii\base\Object;
use app\models\Calendar;
use app\models\Salesproduct;
use app\models\Orderplaced;

class Functions extends Component
{
    public function RequiredFieldVaildation($postArray, $validateArray)
    {
        if ($postArray) {
            foreach ($validateArray as $key => $value) {
                if (array_key_exists($key, $postArray)) {
                    if ($postArray[$key] == '') {
                        $errors[] = $validateArray[$key];
                    }
                } else {
                    $errors[] = $validateArray[$key];
                }
            }
        } else {
            foreach ($validateArray as $key => $value) {
                $errors[] = $validateArray[$key];
            }
        }
        if (!empty($errors)) {
            return implode(',', $errors);
        } else {
            return false;
        }
    }

    public function password()
    {
        $alphabet = '1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 4; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
    public function username($len)
    {
        $string = "";
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for ($i=0;$i<$len;$i++) {
            $string.=substr($chars, rand(0, strlen($chars)), 1);
        }
        return $string;
    }
    public function resize_crop_image($max_width, $max_height, $source_file, $dst_dir, $quality = 50){
        $imgsize = getimagesize($source_file);
        $width = $imgsize[0];
        $height = $imgsize[1];
        $mime = $imgsize['mime'];
      
        switch($mime){
        case 'image/gif':
        $image_create = "imagecreatefromgif";
        $image = "imagegif";
        break;
      
        case 'image/png':
        $image_create = "imagecreatefrompng";
        $image = "imagepng";
        $quality = 7;
        break;
      
        case 'image/jpeg':
        $image_create = "imagecreatefromjpeg";
        $image = "imagejpeg";
        $quality = 80;
        break;
      
        default:
        return false;
        break;
        }
      
        $dst_img = imagecreatetruecolor($max_width, $max_height);
        $src_img = $image_create($source_file);
      
        $width_new = $height * $max_width / $max_height;
        $height_new = $width * $max_height / $max_width;
        //if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
        if($width_new > $width){
        //cut point by height
        $h_point = (($height - $height_new) / 2);
        //copy image
        imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
        }else{
        //cut point by width
        $w_point = (($width - $width_new) / 2);
        imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
        }
      
        $image($dst_img, $dst_dir, $quality);
      
        if($dst_img)imagedestroy($dst_img);
        if($src_img)imagedestroy($src_img);
        }
       
   
  
}
