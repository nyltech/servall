<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use yii\helpers\BaseHtml;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\data\SqlDataProvider;
use kartik\growl\Growl;
use app\models\Customerdetails;


$this->title = 'Clients';



echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'showPageSummary' => false,
    'id' => 'grid',
    'pjax' => true,
    'striped' => true,
    'hover' => true,
    'exportConfig' => [
        GridView::EXCEL => [
            'filename' => 'Customer Details',
        ],

        
    ],
    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'toolbar' => [
        ['content'=>
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['customerdetails'], ['data-pjax'=>0, 'class'=>'btn btn-default', 'title'=> 'Reset Grid'])
        ],
        '{export}',
        '{toggleData}',
    ],
    'panel' => ['type' => 'primary', 'heading' => 'Customer Details'],
    'columns' => [
        [
            'label' => 'Customer Name',
            'value' => 'customerName',
        ],
        [
            'label' => 'Mobile Number',
            'value' => 'mobileNumber',
        ],
        
        
        
       
    ]
]);
?>
